package com.example.applicationmobiletp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class JSONResponseHandlerTeamEvent {
    private static final String TAG = JSONResponseHandlerTeamEvent.class.getSimpleName();
    private Match match;
    JSONResponseHandlerTeamEvent(Match match) {
        this.match = match;
    }

    /**
     * @param response done by the Web service
     * @return A Match with attributes filled with the collected information if response was
     * successfully analyzed
     */
    void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLastEvent(reader);
        } finally {
            reader.close();
        }
    }

    private void readLastEvent(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results") && reader.peek()== JsonToken.BEGIN_ARRAY) {
                readArrayLastEvent(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayLastEvent(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    switch (name) {
                        case "idEvent":
                            match.setId(reader.nextLong());
                            break;
                        case "strEvent":
                            match.setLabel(reader.nextString());
                            break;
                        case "strHomeTeam":
                            match.setHomeTeam(reader.nextString());
                            break;
                        case "strAwayTeam":
                            match.setAwayTeam(reader.nextString());
                            break;
                        case "intHomeScore":
                            if(reader.peek()!=JsonToken.NULL){
                                match.setHomeScore(reader.nextInt());
                            }
                            else{
                                match.setHomeScore(-1);
                                reader.nextNull();
                            }
                            break;
                        case "intAwayScore":
                            if(reader.peek()!=JsonToken.NULL){
                                match.setAwayScore(reader.nextInt());
                            }
                            else{
                                match.setAwayScore(-1);
                                reader.nextNull();
                            }
                            break;
                        default:
                            reader.skipValue();
                            break;
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
