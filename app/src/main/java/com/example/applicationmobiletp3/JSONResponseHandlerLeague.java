package com.example.applicationmobiletp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class JSONResponseHandlerLeague {
    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();
    private Team team;

    JSONResponseHandlerLeague(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLeague(reader);
        } finally {
            reader.close();
        }
    }

    private void readLeague(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table") && reader.peek()== JsonToken.BEGIN_ARRAY) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nbLine=1;
        while (reader.hasNext() ) {
            reader.beginObject();
            String name = reader.nextName();
            if(name.equals("name") && reader.nextString().equals(team.getName())){
                while (reader.hasNext()) {
                    name=reader.nextName();
                    if ("total".equals(name)) {
                        team.setTotalPoints(reader.nextInt());
                        team.setRanking(nbLine);
                    } else {
                        reader.skipValue();
                    }
                }
            }
            else{
                while(reader.hasNext()){
                    reader.skipValue();
                }
            }
            reader.endObject();
            nbLine++;
        }
        reader.endArray();
    }
}
