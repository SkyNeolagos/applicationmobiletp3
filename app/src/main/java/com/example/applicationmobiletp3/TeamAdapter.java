package com.example.applicationmobiletp3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamViewHolder> {
    private List<Team> teamListAdapter;

    public TeamAdapter(List<Team> teamListAdapter) {
        this.teamListAdapter = teamListAdapter;
    }

    public void setTeamListAdapter(List<Team> teamListAdapter) {
        this.teamListAdapter = teamListAdapter;
    }

    @NonNull
    @Override
    public TeamAdapter.TeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.item_team,parent,false);
        return new TeamViewHolder(view,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull TeamAdapter.TeamViewHolder holder, int position) {
        try {
            holder.display(teamListAdapter.get(position));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return teamListAdapter.size();
    }


    static class TeamViewHolder extends RecyclerView.ViewHolder {
        private TextView teamNameHolder;
        private TextView teamLeagueHolder;
        private ImageView teamImageHolder;
        private TextView teamStadeHolder;
        private Context context;

        TeamViewHolder(@NonNull View itemView,Context context) {
            super(itemView);
            teamNameHolder=itemView.findViewById(R.id.teamNameItem);
            teamLeagueHolder=itemView.findViewById(R.id.teamLeagueItem);
            teamImageHolder=itemView.findViewById(R.id.teamImageItem);
            teamStadeHolder=itemView.findViewById(R.id.teamStadeItem);
            this.context=context;
        }
        void display(Team teamHolder) throws IOException {
            teamNameHolder.setText(teamHolder.getName());
            teamLeagueHolder.setText(teamHolder.getLeague());
            teamStadeHolder.setText(teamHolder.getStadium());
            if(teamHolder.getTeamBadge()!=null && !teamHolder.getTeamBadge().isEmpty()){
                Bitmap bitmap=getThumbnail(teamHolder.getName()+".png");
                if(bitmap!=null){
                    teamImageHolder.setImageBitmap(bitmap);
                }
                else{
                    DownloadImageWithURLTask downloadImageWithURLTask = new DownloadImageWithURLTask();
                    downloadImageWithURLTask.setTeamName(teamHolder.getName());
                    downloadImageWithURLTask.execute(teamHolder.getTeamBadge());
                }
            }
            else{
                teamImageHolder.setImageResource(R.mipmap.ic_launcher);
            }

        }

        @SuppressLint("StaticFieldLeak")
        private class DownloadImageWithURLTask extends AsyncTask<String, Void, Bitmap> {
            Bitmap bitmap = null;
            String teamName;

            public void setTeamName(String teamName) {
                this.teamName = teamName;
            }

            @Override
            protected Bitmap doInBackground(String... urls) {
                String pathToFile = urls[0];
                try {
                    InputStream in = new java.net.URL(pathToFile).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", Objects.requireNonNull(e.getMessage()));
                    e.printStackTrace();
                }
                return bitmap;
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
            }
            @Override
            protected void onPostExecute(Bitmap result) {
                saveImageToInternalStorage(bitmap,teamName);
                teamImageHolder.setImageBitmap(result);
            }
        }
        public boolean saveImageToInternalStorage(Bitmap image,String teamName) {
            try {
                FileOutputStream fos = context.openFileOutput(teamName+".png", Context.MODE_PRIVATE);
                image.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
                return true;
            } catch (Exception e) {
                Log.e("saveToInternalStorage()", e.getMessage());
                return false;
            }
        }
        @SuppressLint("LongLogTag")
        public Bitmap getThumbnail(String filename) {
            Bitmap thumbnail = null;
            try {
                File filePath = context.getFileStreamPath(filename);
                FileInputStream fi = new FileInputStream(filePath);
                thumbnail = BitmapFactory.decodeStream(fi);
            } catch (Exception ex) {
                Log.e("getThumbnail() on internal storage", Objects.requireNonNull(ex.getMessage()));
            }
            return thumbnail;
        }
    }
}

