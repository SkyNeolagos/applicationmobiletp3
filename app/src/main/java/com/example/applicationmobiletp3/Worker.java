package com.example.applicationmobiletp3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Worker extends AsyncTask<URL,Integer,Team> {
    private Team team;
    private Boolean notInAPI=false;
    @SuppressLint("StaticFieldLeak")
    private Context context;

    void setContext(Context context) {
        this.context=context;
    }
    void setTeam(Team team) {
        this.team = team;
    }
    @Override
    protected Team doInBackground(URL... urls) {
        InputStream inputStream;
        JSONResponseHandlerTeam jsonResponseHandlerTeam=new JSONResponseHandlerTeam(this.team);
        HttpURLConnection httpURLConnectionTeamResponse;


        Match match=new Match();
        this.team.setLastEvent(match);
        JSONResponseHandlerTeamEvent jsonResponseHandlerTeamEvent=new JSONResponseHandlerTeamEvent(match);
        HttpURLConnection httpURLConnectionEventResponse;

        JSONResponseHandlerLeague jsonResponseHandlerLeague=new JSONResponseHandlerLeague(this.team);
        HttpURLConnection httpURLConnectionLeagueResponse;
        try {
            httpURLConnectionTeamResponse = (HttpURLConnection) urls[0].openConnection();
            try {
                inputStream=new BufferedInputStream(httpURLConnectionTeamResponse.getInputStream());
                try{
                    //Bloc MAJ Nom, idLeague, Stade, Localisation
                    jsonResponseHandlerTeam.readJsonStream(inputStream);
                    notInAPI=jsonResponseHandlerTeam.getNotInAPI();
                    if(!notInAPI){
                        try {
                            URL urlLastEvent=WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                            try{
                                httpURLConnectionEventResponse=(HttpURLConnection) urlLastEvent.openConnection();
                                try{
                                    inputStream=new BufferedInputStream(httpURLConnectionEventResponse.getInputStream());
                                    try {
                                        //Bloc MAJ LastEvent
                                        jsonResponseHandlerTeamEvent.readJsonStream(inputStream);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        try{
                            URL urlLeague=WebServiceUrl.buildGetRanking(team.getIdLeague());
                            try{
                                httpURLConnectionLeagueResponse=(HttpURLConnection) urlLeague.openConnection();
                                try{
                                    inputStream=new BufferedInputStream(httpURLConnectionLeagueResponse.getInputStream());
                                    try{
                                        //Bloc MAJ Ranking et Point
                                        jsonResponseHandlerLeague.readJsonStream(inputStream);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return team;
    }

    @Override
    protected void onPostExecute(Team team) {
        super.onPostExecute(team);
        if(context instanceof TeamActivity) {//Verification Worker lancer dans une TeamActivity
            if (notInAPI) {
                Toast toast = Toast.makeText(context, "Ce nom d'équipe n'existe pas dans l'API", Toast.LENGTH_LONG);
                toast.show();
            } else {
                Toast toast = Toast.makeText(context, "Fin de la Maj", Toast.LENGTH_LONG);
                toast.show();
                ((TeamActivity) context).updateView();
            }
        }
        else if(context instanceof MainActivity){
            ((MainActivity) context).sportDbHelper.updateTeam(team);
            ((MainActivity) context).multiWorker.taskComplete();
        }
    }
}