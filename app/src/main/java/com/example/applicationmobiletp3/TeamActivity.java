package com.example.applicationmobiletp3;




import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Objects;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    private Worker worker=new Worker();
    ConnectivityManager connectivityManager;
    NetworkInfo activeNetworkInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = getIntent().getParcelableExtra(Team.TAG);

        textTeamName = findViewById(R.id.nameTeam);
        textLeague = findViewById(R.id.league);
        textStadium = findViewById(R.id.editStadium);
        textStadiumLocation = findViewById(R.id.editStadiumLocation);
        textTotalScore = findViewById(R.id.editTotalScore);
        textRanking = findViewById(R.id.editRanking);
        textLastMatch = findViewById(R.id.editLastMatch);
        textLastUpdate = findViewById(R.id.editLastUpdate);

        imageBadge = findViewById(R.id.imageView);

        updateView();

        final Button but = findViewById(R.id.button);

        but.setOnClickListener(v -> {
            connectivityManager= (ConnectivityManager) TeamActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connectivityManager != null;
            activeNetworkInfo= connectivityManager.getActiveNetworkInfo();
            if(activeNetworkInfo !=null && activeNetworkInfo.isConnectedOrConnecting()){
                    if(worker.getStatus() == AsyncTask.Status.PENDING){
                        worker.setTeam(team);
                        worker.setContext(TeamActivity.this);
                        try {
                            worker.execute(WebServiceUrl.buildSearchTeam(team.getName()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(worker.getStatus()==AsyncTask.Status.FINISHED){
                        Toast toast = Toast.makeText(TeamActivity.this, "Maj déjà faite.", Toast.LENGTH_LONG);
                        toast.show();
                    }
            }
            else{
                Toast toast = Toast.makeText(TeamActivity.this, "Aucune Connection", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        if(team!=null){
            intent.putExtra(Team.TAG,team);
            setResult(RESULT_OK,intent);
            finish();
        }
        else{
            setResult(RESULT_CANCELED,intent);
            finish();
        }
        super.onBackPressed();
    }

    @SuppressLint("SetTextI18n")
    void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if(team.getTeamBadge()!=null && !team.getTeamBadge().isEmpty()){
            ConnectivityManager connectivityManager = (ConnectivityManager) TeamActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connectivityManager != null;
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            Bitmap bitmap=getThumbnail(team.getName()+".png");
            if(bitmap!=null){
                imageBadge.setImageBitmap(bitmap);
            }
            else{
                if(team.getTeamBadge()!=null && !team.getTeamBadge().isEmpty()){
                    if(activeNetworkInfo !=null && activeNetworkInfo.isConnectedOrConnecting()){
                        DownloadImageWithURLTask downloadImageWithURLTask = new DownloadImageWithURLTask();
                        downloadImageWithURLTask.setTeamName(team.getName());
                        downloadImageWithURLTask.execute(team.getTeamBadge());
                    }
                }
                else{
                    imageBadge.setImageResource(R.mipmap.ic_launcher);
                }
            }
        }

    }
    @SuppressLint("StaticFieldLeak")
    private class DownloadImageWithURLTask extends AsyncTask<String, Void, Bitmap> {
        Bitmap bitmap = null;
        String teamName;

        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String pathToFile = urls[0];
            try {
                InputStream in = new java.net.URL(pathToFile).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", Objects.requireNonNull(e.getMessage()));
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            saveImageToInternalStorage(bitmap,teamName);
            imageBadge.setImageBitmap(result);
        }
    }
    public boolean saveImageToInternalStorage(Bitmap image,String teamName) {
        try {
            FileOutputStream fos = TeamActivity.this.openFileOutput(teamName+".png", Context.MODE_PRIVATE);
            System.out.println("Hello");
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            return false;
        }
    }
    @SuppressLint("LongLogTag")
    public Bitmap getThumbnail(String filename) {
        Bitmap thumbnail = null;
        try {
            File filePath = TeamActivity.this.getFileStreamPath(filename);
            FileInputStream fi = new FileInputStream(filePath);
            thumbnail = BitmapFactory.decodeStream(fi);
        } catch (Exception ex) {
            Log.e("getThumbnail() on internal storage", Objects.requireNonNull(ex.getMessage()));
        }
        return thumbnail;
    }
}

