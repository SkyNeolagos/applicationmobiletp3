package com.example.applicationmobiletp3;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    static final int PICK_TEAM_REQUEST = 1;
    static final int PICK_UPDATE_REQUEST = 2;
    MultiWorker multiWorker=null;
    SportDbHelper sportDbHelper=null;
    ConnectivityManager connectivityManager;
    NetworkInfo activeNetworkInfo;
    private List<Team> teamList=null;
    private RecyclerView recyclerView;
    private TeamAdapter teamAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final SwipeRefreshLayout swipeRefreshLayout= findViewById(R.id.swipeRefresh);

        sportDbHelper=new SportDbHelper(this);
        if (sportDbHelper.fetchAllTeams().getCount() == 0) {
            sportDbHelper.populate();
        }

        recyclerView=findViewById(R.id.teamListRecyclerView);
        teamList=sportDbHelper.getAllTeams();

        teamAdapter=new TeamAdapter(teamList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(teamAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this.getBaseContext(), recyclerView , (view, position) -> {
                    Intent intent= new Intent(MainActivity.this,TeamActivity.class);
                    intent.putExtra(Team.TAG,teamList.get(position));
                    startActivityForResult(intent,PICK_UPDATE_REQUEST);
                })
        );

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intentNewTeam=new Intent(MainActivity.this,NewTeamActivity.class);
            startActivityForResult(intentNewTeam,PICK_TEAM_REQUEST);
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {
            connectivityManager = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connectivityManager != null;
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
                if (teamList != null) {
                    if(teamList.size()==0){
                        swipeRefreshLayout.setRefreshing(false);
                        Toast toast = Toast.makeText(MainActivity.this, "Rien a actualiser !!", Toast.LENGTH_LONG);
                        toast.show();
                    }else{
                        int totalNumOfTasks = teamList.size();
                        multiWorker = new MultiWorker(totalNumOfTasks) {
                            @Override
                            protected void onAllTasksCompleted() {
                                swipeRefreshLayout.setRefreshing(false);
                                refresh();
                            }
                        };
                        for (int i = 0; i < teamList.size(); i++) {
                            Team team = teamList.get(i);
                            Worker worker = new Worker();
                            worker.setTeam(team);
                            worker.setContext(MainActivity.this);
                            try {
                                worker.execute(WebServiceUrl.buildSearchTeam(team.getName()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                swipeRefreshLayout.setRefreshing(false);
                Toast toast = Toast.makeText(MainActivity.this, "Aucune Connection", Toast.LENGTH_LONG);
                toast.show();
            }
        });


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        ItemTouchHelper.Callback itemToucherHelperCallback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                sportDbHelper.getAllTeams().remove(position);
                sportDbHelper.deleteTeam(teamList.get(position).getId());
                teamAdapter.notifyItemRemoved(position);
                refresh();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemToucherHelperCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICK_TEAM_REQUEST:
                if (resultCode == RESULT_OK) {
                    Team team= data.getParcelableExtra(Team.TAG);
                    if(team!=null){
                        sportDbHelper.addTeam(team);
                        refresh();
                    }
                }
            case PICK_UPDATE_REQUEST:
                if(resultCode==RESULT_OK){
                    Team team= data.getParcelableExtra(Team.TAG);
                    if(team!=null){
                        sportDbHelper.updateTeam(team);
                        refresh();
                    }
                }
        }
    }

    void refresh(){
        teamList=sportDbHelper.getAllTeams();
        teamAdapter.setTeamListAdapter(teamList);
        recyclerView.setAdapter(teamAdapter);
    }
}
