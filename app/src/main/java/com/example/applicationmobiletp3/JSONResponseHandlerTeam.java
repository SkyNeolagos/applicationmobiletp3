package com.example.applicationmobiletp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;

    Boolean getNotInAPI() {
        return notInAPI;
    }

    private Boolean notInAPI=false;

    JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    private void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("teams") && reader.peek()==JsonToken.BEGIN_ARRAY) {
                readArrayTeams(reader);
            } else {
                notInAPI=true;
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    switch (name) {
                        case "idTeam":
                            team.setIdTeam(reader.nextLong());
                            break;
                        case "strTeam":
                            team.setName(reader.nextString());
                            break;
                        case "strLeague":
                            team.setLeague(reader.nextString());
                            break;
                        case "idLeague":
                            team.setIdLeague(reader.nextLong());
                            break;
                        case "strStadium":
                            team.setStadium(reader.nextString());
                            break;
                        case "strStadiumLocation":
                            team.setStadiumLocation(reader.nextString());
                            break;
                        case "strTeamBadge":
                            team.setTeamBadge(reader.nextString());
                            break;
                        default:
                            reader.skipValue();
                            break;
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}

